import eikon as ek
import pandas as pd
from datetime import date

from Globals import excel
from Globals import helpers

# Styles for excel sheet


# apply the formats
# the location of the this method is findable in excel.py
def apply_formats(worksheet, df, formats):

    """

    The functions applies the passed formats coming from add.formats

    :type worksheet: xlsx-writer worksheet
    :param worksheet: worksheet in the current loop
    :type df: pandas DataFrame
    :param df: DataFrame containing the data for the passed worksheet
    :type formats: list of dictionaries
    :param formats: styles

    """

    # get the number of rows
    number_rows = len(df.index)

    # increase the column width
    worksheet.set_column('C:C', 40)
    worksheet.set_column('D:D', 40)

    # decrease the column width
    worksheet.set_column('B:B', 15)
    worksheet.set_column('E:P', 15)

    # hide the first row
    #worksheet.set_row(1,0,0, {'hidden':True})

    conditional_format_range = "Q2:Q{}".format(number_rows)


    # FAILED cells are red
    worksheet.conditional_format(conditional_format_range,
                                 {'type': 'text',
                                    'criteria': 'containing',
                                    'value': 'FAILED',
                                    'format': formats[1]})

    # PASSED cells are green
    worksheet.conditional_format(conditional_format_range,
                                 {'type': 'text',
                                  'criteria': 'containing',
                                  'value': 'PASSED',
                                  'format': formats[2]})


# add Styles
def add_formats():

    """
    Sets the specific styles for worksheets.

    :rtype: list of dictionaries
    :return: styles to be applied
    """

    styles = [{'font_size' : 11,
               'bold': True,
               'align': 'center',
               'valign': 'vcenter'
               },{'bg_color': '#FFC7CE'},
              {'bg_color': '#C6EFCE',
               'font_color': '#006100'}
              ]
    return styles


# Set App Key for Eikon Connection
ek.set_app_key('xxx')

# Get RICs from Robin ETF Universe
etf_ric = pd.read_csv(r'V:\CO-Robin-Team\21 - Product Universe\RIC_Eikon.csv', sep=';')

#help(ek.get_data)

# Set the required fields

fields = [ek.TR_Field('tr.FundBenchmarkName'), ek.TR_Field('tr.CommonName'), ek.TR_Field('tr.MarketCapLocalCurn'),
          ek.TR_Field('tr.FundLaunchDate'), ek.TR_Field('tr.AvgDailyValTraded30D'), ek.TR_Field('tr.FundTrackingError1Year'),
          ek.TR_Field('tr.FundTrackingError3Year'), ek.TR_Field('tr.RelPricePctChg52W'), ek.TR_Field('tr.TotalReturn52Wk')]

data, err = ek.get_data(etf_ric['RIC'].tolist(), fields)

# Add ISINs
data['ISIN'] = etf_ric['ISIN']

# Calculate the Net Tracking Difference
data['Net Tracking Difference'] = data['52 Week Total Return'] - data['Benchmark Relative 52-week Price PCT Change']

# Rename columns
data = data.rename(columns=({'Launch Date':'Inception Date','Company Common Name':'Title',\
                             'Company Market Cap':'AuM EUR','Average Daily Value Traded - 30 Days':'Daily traded volume'}))

# Drop irrelevant columns and rearrange
data_clean = data[['ISIN', 'Instrument','Benchmark Name','Title','AuM EUR', 'Inception Date',\
                   'Daily traded volume','Tracking Error for 1 Year to Last Month End',\
                   'Tracking Error for 3 Years to Last Month End','Net Tracking Difference']]

# Calculate Track Record of Funds
date.today()
data_clean['Timedelta'] = date.today()-pd.to_datetime(data_clean['Inception Date'])
data_clean['Timedelta_int'] = data_clean['Timedelta'].dt.days

# PASSED FAILED Conditions
data_clean.loc[(data_clean['AuM EUR'] > 50000000), 'AuM > 50 Mio'] = 'PASSED'
data_clean.loc[(data_clean['Timedelta_int'] > 6*31), 'Track Record > 6months'] = 'PASSED'
data_clean.loc[(data_clean['Daily traded volume'] > 500000), 'Volume > 500.000'] = 'PASSED'
data_clean.loc[(data_clean['Tracking Error for 1 Year to Last Month End'] < 1), 'Tracking Error < 100bps 1Yr'] = 'PASSED'
data_clean.loc[(data_clean['Tracking Error for 3 Years to Last Month End'] < 1), 'Tracking Error < 100bps last 3 Yrs'] = 'PASSED'
data_clean.loc[(data_clean['Net Tracking Difference']>-1.5), 'Net Tracking Difference < -150bps 1 year'] = 'PASSED'

# Fill NaN with 'FAILED'
data_clean[['AuM > 50 Mio', 'Volume > 500.000', 'Track Record > 6months',
       'Tracking Error < 100bps 1Yr', 'Tracking Error < 100bps last 3 Yrs',
       'Net Tracking Difference < -150bps 1 year']]= data_clean[['AuM > 50 Mio', 'Volume > 500.000', 'Track Record > 6months',
       'Tracking Error < 100bps 1Yr', 'Tracking Error < 100bps last 3 Yrs',
       'Net Tracking Difference < -150bps 1 year']].fillna('FAILED')


# Overall condition to have minimum 2 'PASSED'
for i in range(len(data_clean)):
    data_clean['Overall'] = 'PASSED' if data_clean.iloc[i].tolist().count('PASSED')>=2 else 'FAILED'

# Clean the data
data_clean['AuM in Mio EUR'] = (data_clean['AuM EUR']/1000000).astype(int)
data_clean['Tracking Error for 1 Year to Last Month End'] = data_clean['Tracking Error for 1 Year to Last Month End']\
    .astype(float).map('{:,.4f}'.format)
data_clean['Tracking Error for 3 Years to Last Month End'] = data_clean['Tracking Error for 3 Years to Last Month End']\
    .astype(float).map('{:,.4f}'.format)
data_clean['Net Tracking Difference'] = data_clean['Net Tracking Difference']\
    .astype(float).map('{:,.4f}'.format)
data_clean['Daily traded volume'] = data_clean['Daily traded volume'].astype(int)

# Drop irrelevant columns and rearrange
data_final = data_clean[['ISIN', 'Instrument','Benchmark Name','Title','AuM in Mio EUR', 'Inception Date',
                         'Daily traded volume','Tracking Error for 1 Year to Last Month End',
                         'Tracking Error for 3 Years to Last Month End','Net Tracking Difference',
                        'AuM > 50 Mio', 'Volume > 500.000', 'Track Record > 6months',
       'Tracking Error < 100bps 1Yr', 'Tracking Error < 100bps last 3 Yrs',
       'Net Tracking Difference < -150bps 1 year', 'Overall']]

# EXPORT ALL PIVOTS TO A SINGLE EXCEL FILE -----------------------------------------------------------

today = helpers.get_current_date()

export_name = '{} IPI_Liste'.format(today)

excel.export_to_excel(export_name,
                      add_formats, apply_formats,
                      dfs=[

                          {'sheetname': 'IPI Liste', 'dataframe': data_final}

                      ]
                      )




