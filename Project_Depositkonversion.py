#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# if using a Jupyter notebook, includue:
get_ipython().run_line_magic('matplotlib', 'inline')
from pandas.api.types import CategoricalDtype
from plotnine import *

import warnings

warnings.filterwarnings("ignore", category=UserWarning)

from sklearn.model_selection import GridSearchCV
# Import the required packages
from sklearn.tree import DecisionTreeClassifier

# ## Import of data

# In[ ]:


# Import EKL
data = pd.read_csv(r'H:\Robin\DataScience\Adj_EKL_Mittelherkunft_Robin.csv')
data.head()
# len(data)


# In[ ]:


# Import contract information from Robo Databank
contract = pd.read_csv(r'H:\Robin\DataScience\contract.csv')
contract.head()
# len(contract)


# In[ ]:


# Merge data sets
temp = data.merge(contract, on='FKN')
temp.head()

# In[ ]:


# Import & merge risk corridor
risk = pd.read_csv(r'H:\Robin\DataScience\20200902_risk_corridor.csv', sep=";")
temp2 = temp.merge(risk, on='FKN')
temp2.head()

# ## Info about data (types, structure, nans, etc.)

# In[ ]:


temp2.info()

# In[ ]:


# Fill missing values for region/filiale

temp2['region'] = temp2['region'].fillna('FIL_BERLIN-Quartier Zukunft')
temp2['filiale'] = temp2['filiale'].fillna('25 PARTNERSHIP')
temp2['GESCHLECHT'] = temp2['GESCHLECHT'].fillna('None')
temp2['filiale'].isnull().sum()

# In[ ]:


# Convert str to float
temp2['PF_TOTAL'] = temp2['PF_TOTAL'].replace("-", "0", regex=True)
temp2['PF_TOTAL'] = temp2['PF_TOTAL'].replace(",", ".", regex=True).astype(float)
temp2['PF_CASH'] = temp2['PF_CASH'].replace("-", "0", regex=True)
temp2['PF_CASH'] = temp2['PF_CASH'].replace(",", ".", regex=True).astype(float)
temp2['KUNDENBINDUNG_DAUER'] = temp2['KUNDENBINDUNG_DAUER'].replace(".", "0").astype(float)
temp2['einkommen_echt'] = temp2['einkommen_echt'].replace(".", "0").astype(float)
temp2['VOLUMEN_DEPOT'] = temp2['VOLUMEN_DEPOT'].replace("-", "0", regex=True)
temp2['VOLUMEN_DEPOT'] = temp2['VOLUMEN_DEPOT'].replace(",", ".", regex=True).astype(float)
temp2['VOLUMEN_EINLAGE'] = temp2['VOLUMEN_EINLAGE'].replace("-", "0", regex=True)
temp2['VOLUMEN_EINLAGE'] = temp2['VOLUMEN_EINLAGE'].replace(",", ".", regex=True).astype(float)

# Convert creationdate ('Eröffnungsdatum') to datetime object
# New dateformat Y-m-d, e.g. 2020-05-31
temp2['CN_CREATED_AT_DATE'] = pd.to_datetime(temp2['CN_CREATED_AT'], format="%d.%m.%y")

# In[ ]:


temp2.info()

# In[ ]:


# Convert floating type to categorical variable
temp2['max_einlagen_ges'] = pd.Categorical(temp2['max_einlagen_ges'])
temp2['max_freshmoney_ges'] = pd.Categorical(temp2['max_freshmoney_ges'])
temp2['max_depot_ges'] = pd.Categorical(temp2['max_depot_ges'])

# In[ ]:


# Check for NaN, Outliers
temp2.describe()

# In[ ]:


temp2.columns

# In[ ]:


# # Graphs

# In[ ]:


# Histograms of variables
plt.hist(temp2['ALTER_PARTNER'])
plt.show()

# In[ ]:


temp2['GESCHLECHT'].value_counts().plot(kind='bar')

# In[ ]:


# PLot portfolios
# Note: Robin portfolio is NOT included
plt.hist(temp2['VOLUMEN_DEPOT'], bins=50)
plt.show()

# In[ ]:


# Plot portfolios EXCLUDING portfolios = 0
ex_vol = temp2.loc[temp2['VOLUMEN_DEPOT'] != 0]
plt.hist(ex_vol['VOLUMEN_DEPOT'], bins=50)
plt.show()

# In[ ]:


# Portfolios buckets
temp2['VOLUMEN_DEPOT_SHORT'] = 1
temp2.loc[temp2['VOLUMEN_DEPOT'] == 0, 'VOLUMEN_DEPOT_SHORT'] = '0'
temp2.loc[(temp2['VOLUMEN_DEPOT'] > 0) & (temp2['VOLUMEN_DEPOT'] < 1000), 'VOLUMEN_DEPOT_SHORT'] = '1-1K'
temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 1000) & (temp2['VOLUMEN_DEPOT'] < 5000), 'VOLUMEN_DEPOT_SHORT'] = '1K-5K'
temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 5000) & (temp2['VOLUMEN_DEPOT'] < 10000), 'VOLUMEN_DEPOT_SHORT'] = '5K-10K'
temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 10000) & (temp2['VOLUMEN_DEPOT'] < 50000), 'VOLUMEN_DEPOT_SHORT'] = '10K-50K'
temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 50000) & (temp2['VOLUMEN_DEPOT'] < 100000), 'VOLUMEN_DEPOT_SHORT'] = '50K-100K'
temp2.loc[temp2['VOLUMEN_DEPOT'] >= 100000, 'VOLUMEN_DEPOT_SHORT'] = '100K+'

# In[ ]:


# Portfolios in even buckets
# temp2['VOLUMEN_DEPOT_SHORT'] = '0'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] > 0) & (temp2['VOLUMEN_DEPOT'] < 10000), 'VOLUMEN_DEPOT_SHORT'] = '1-10K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 10000) & (temp2['VOLUMEN_DEPOT'] < 20000), 'VOLUMEN_DEPOT_SHORT'] = '10K-20K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 20000) & (temp2['VOLUMEN_DEPOT'] < 30000), 'VOLUMEN_DEPOT_SHORT'] = '20K-30K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 30000) & (temp2['VOLUMEN_DEPOT'] < 40000), 'VOLUMEN_DEPOT_SHORT'] = '30K-40K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 40000) & (temp2['VOLUMEN_DEPOT'] < 50000), 'VOLUMEN_DEPOT_SHORT'] = '40K-50K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 50000) & (temp2['VOLUMEN_DEPOT'] < 60000), 'VOLUMEN_DEPOT_SHORT'] = '50K-60K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 60000) & (temp2['VOLUMEN_DEPOT'] < 70000), 'VOLUMEN_DEPOT_SHORT'] = '60K-70K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 70000) & (temp2['VOLUMEN_DEPOT'] < 80000), 'VOLUMEN_DEPOT_SHORT'] = '70K-80K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 80000) & (temp2['VOLUMEN_DEPOT'] < 90000), 'VOLUMEN_DEPOT_SHORT'] = '80K-90K'
# temp2.loc[(temp2['VOLUMEN_DEPOT'] >= 90000) & (temp2['VOLUMEN_DEPOT'] < 100000), 'VOLUMEN_DEPOT_SHORT'] = '90K-100K'
# temp2.loc[temp2['VOLUMEN_DEPOT'] >= 100000, 'VOLUMEN_DEPOT_SHORT'] = '100K+'


# In[ ]:


# Plot Portfolio buckets
temp2['VOLUMEN_DEPOT_SHORT'].value_counts().plot(kind='bar')

# In[ ]:


# Income in buckets
temp2['EINKOMMEN_SHORT'] = '0'
temp2.loc[(temp2['einkommen_echt'] > 0) & (temp2['einkommen_echt'] < 2000), 'EINKOMMEN_SHORT'] = '1-2K'
temp2.loc[(temp2['einkommen_echt'] >= 2000) & (temp2['einkommen_echt'] < 4000), 'EINKOMMEN_SHORT'] = '2K-4K'
temp2.loc[(temp2['einkommen_echt'] >= 4000) & (temp2['einkommen_echt'] < 6000), 'EINKOMMEN_SHORT'] = '4K-6K'
temp2.loc[(temp2['einkommen_echt'] >= 6000) & (temp2['einkommen_echt'] < 8000), 'EINKOMMEN_SHORT'] = '6K-8K'
temp2.loc[(temp2['einkommen_echt'] >= 8000) & (temp2['einkommen_echt'] < 10000), 'EINKOMMEN_SHORT'] = '8K-10K'
temp2.loc[temp2['einkommen_echt'] >= 10000, 'EINKOMMEN_SHORT'] = '10K+'

# In[ ]:


# Age in buckets
temp2['ALTER_SHORT'] = '0'
temp2.loc[(temp2['ALTER_PARTNER'] < 18), 'ALTER_SHORT'] = '<18'
temp2.loc[(temp2['ALTER_PARTNER'] >= 18) & (temp2['ALTER_PARTNER'] < 30), 'ALTER_SHORT'] = '18-30'
temp2.loc[(temp2['ALTER_PARTNER'] >= 30) & (temp2['ALTER_PARTNER'] < 40), 'ALTER_SHORT'] = '30-40'
temp2.loc[(temp2['ALTER_PARTNER'] >= 40) & (temp2['ALTER_PARTNER'] < 50), 'ALTER_SHORT'] = '40-50'
temp2.loc[(temp2['ALTER_PARTNER'] >= 50) & (temp2['ALTER_PARTNER'] < 60), 'ALTER_SHORT'] = '50-60'
temp2.loc[(temp2['ALTER_PARTNER'] >= 60) & (temp2['ALTER_PARTNER'] < 70), 'ALTER_SHORT'] = '60-70'
temp2.loc[temp2['ALTER_PARTNER'] >= 70, 'ALTER_SHORT'] = '>70'

# In[ ]:


# Plot Income Buckets
temp2['EINKOMMEN_SHORT'].value_counts().plot(kind='bar')

# ### Sicherungskopie des Dataframes

# In[ ]:


temp3 = temp2.copy()
temp3.head()

# In[ ]:


# Plot deposits
plt.hist(temp3['VOLUMEN_EINLAGE'], bins=20)
plt.show()

# In[ ]:


# Deposits in buckets for normal distribution
temp3['VOLUMEN_EINLAGE_SHORT'] = 1
temp3.loc[temp3['VOLUMEN_EINLAGE'] == 0, 'VOLUMEN_EINLAGE_SHORT'] = '0'
temp3.loc[(temp3['VOLUMEN_EINLAGE'] > 0) & (temp3['VOLUMEN_EINLAGE'] < 1000), 'VOLUMEN_EINLAGE_SHORT'] = '1-1K'
temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 1000) & (temp3['VOLUMEN_EINLAGE'] < 5000), 'VOLUMEN_EINLAGE_SHORT'] = '1K-5K'
temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 5000) & (temp3['VOLUMEN_EINLAGE'] < 10000), 'VOLUMEN_EINLAGE_SHORT'] = '5K-10K'
temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 10000) & (temp3['VOLUMEN_EINLAGE'] < 50000), 'VOLUMEN_EINLAGE_SHORT'] = '10K-50K'
temp3.loc[
    (temp3['VOLUMEN_EINLAGE'] >= 50000) & (temp3['VOLUMEN_EINLAGE'] < 100000), 'VOLUMEN_EINLAGE_SHORT'] = '50K-100K'
temp3.loc[temp3['VOLUMEN_EINLAGE'] >= 100000, 'VOLUMEN_EINLAGE_SHORT'] = '100K+'

# In[ ]:


# Deposits in even buckets
# temp3['VOLUMEN_EINLAGE_SHORT'] = '0'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] > 0) & (temp3['VOLUMEN_EINLAGE'] < 10000), 'VOLUMEN_EINLAGE_SHORT'] = '1-10K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 10000) & (temp3['VOLUMEN_EINLAGE'] < 20000), 'VOLUMEN_EINLAGE_SHORT'] = '10K-20K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 20000) & (temp3['VOLUMEN_EINLAGE'] < 30000), 'VOLUMEN_EINLAGE_SHORT'] = '20K-30K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 30000) & (temp3['VOLUMEN_EINLAGE'] < 40000), 'VOLUMEN_EINLAGE_SHORT'] = '30K-40K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 40000) & (temp3['VOLUMEN_EINLAGE'] < 50000), 'VOLUMEN_EINLAGE_SHORT'] = '40K-50K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 50000) & (temp3['VOLUMEN_EINLAGE'] < 60000), 'VOLUMEN_EINLAGE_SHORT'] = '50K-60K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 60000) & (temp3['VOLUMEN_EINLAGE'] < 70000), 'VOLUMEN_EINLAGE_SHORT'] = '60K-70K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 70000) & (temp3['VOLUMEN_EINLAGE'] < 80000), 'VOLUMEN_EINLAGE_SHORT'] = '70K-80K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 80000) & (temp3['VOLUMEN_EINLAGE'] < 90000), 'VOLUMEN_EINLAGE_SHORT'] = '80K-90K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 90000) & (temp3['VOLUMEN_EINLAGE'] < 100000), 'VOLUMEN_EINLAGE_SHORT'] = '90K-100K'
# temp3.loc[(temp3['VOLUMEN_EINLAGE'] >= 100000) & (temp3['VOLUMEN_EINLAGE'] < 200000), 'VOLUMEN_EINLAGE_SHORT'] = '100K-200K'
# temp3.loc[temp3['VOLUMEN_EINLAGE'] >= 200000, 'VOLUMEN_EINLAGE_SHORT'] = '200K+'


# In[ ]:


# Plot deposits
temp3['VOLUMEN_EINLAGE_SHORT'].value_counts().plot(kind='bar')

# In[ ]:


# ROBIN portfolios in buckets
temp3['ROBIN_DEPOT_SHORT'] = '0'
temp3.loc[(temp3['PF_TOTAL'] >= 0) & (temp3['PF_TOTAL'] < 500), 'ROBIN_DEPOT_SHORT'] = '0-0.5K'
temp3.loc[(temp3['PF_TOTAL'] >= 500) & (temp3['PF_TOTAL'] < 1000), 'ROBIN_DEPOT_SHORT'] = '0.5K-1K'
temp3.loc[(temp3['PF_TOTAL'] >= 1000) & (temp3['PF_TOTAL'] < 5000), 'ROBIN_DEPOT_SHORT'] = '1K-5K'
temp3.loc[(temp3['PF_TOTAL'] >= 5000) & (temp3['PF_TOTAL'] < 10000), 'ROBIN_DEPOT_SHORT'] = '5K-10K'
temp3.loc[(temp3['PF_TOTAL'] >= 10000) & (temp3['PF_TOTAL'] < 50000), 'ROBIN_DEPOT_SHORT'] = '10K-50K'
temp3.loc[(temp3['PF_TOTAL'] >= 50000) & (temp3['PF_TOTAL'] < 100000), 'ROBIN_DEPOT_SHORT'] = '50K-100K'
temp3.loc[temp3['PF_TOTAL'] >= 100000, 'ROBIN_DEPOT_SHORT'] = '100K+'

# In[ ]:


# Robin portfolios in even buckets
# temp3['ROBIN_DEPOT_SHORT'] = '0'
# temp3.loc[(temp3['PF_TOTAL'] > 0) & (temp3['PF_TOTAL'] < 5000), 'ROBIN_DEPOT_SHORT'] = '1-5K'
# temp3.loc[(temp3['PF_TOTAL'] > 5000) & (temp3['PF_TOTAL'] < 10000), 'ROBIN_DEPOT_SHORT'] = '5-10K'
# temp3.loc[(temp3['PF_TOTAL'] >= 10000) & (temp3['PF_TOTAL'] < 20000), 'ROBIN_DEPOT_SHORT'] = '10K-20K'
# temp3.loc[(temp3['PF_TOTAL'] >= 20000) & (temp3['PF_TOTAL'] < 30000), 'ROBIN_DEPOT_SHORT'] = '20K-30K'
# temp3.loc[(temp3['PF_TOTAL'] >= 30000) & (temp3['PF_TOTAL'] < 40000), 'ROBIN_DEPOT_SHORT'] = '30K-40K'
# temp3.loc[(temp3['PF_TOTAL'] >= 40000) & (temp3['PF_TOTAL'] < 50000), 'ROBIN_DEPOT_SHORT'] = '40K-50K'
# temp3.loc[(temp3['PF_TOTAL'] >= 50000) & (temp3['PF_TOTAL'] < 60000), 'ROBIN_DEPOT_SHORT'] = '50K-60K'
# temp3.loc[(temp3['PF_TOTAL'] >= 60000) & (temp3['PF_TOTAL'] < 70000), 'ROBIN_DEPOT_SHORT'] = '60K-70K'
# temp3.loc[(temp3['PF_TOTAL'] >= 70000) & (temp3['PF_TOTAL'] < 80000), 'ROBIN_DEPOT_SHORT'] = '70K-80K'
# temp3.loc[(temp3['PF_TOTAL'] >= 80000) & (temp3['PF_TOTAL'] < 90000), 'ROBIN_DEPOT_SHORT'] = '80K-90K'
# temp3.loc[(temp3['PF_TOTAL'] >= 90000) & (temp3['PF_TOTAL'] < 100000), 'ROBIN_DEPOT_SHORT'] = '90K-100K'
# temp3.loc[(temp3['PF_TOTAL'] >= 100000) & (temp3['PF_TOTAL'] < 200000), 'ROBIN_DEPOT_SHORT'] = '100K-200K'
# temp3.loc[temp3['PF_TOTAL'] >= 200000, 'ROBIN_DEPOT_SHORT'] = '200K+'


# In[ ]:


temp3['CN_CREATED_SHORT'] = '0'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2017-07-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2017-10-01'), 'CN_CREATED_SHORT'] = '2017Q3'
temp3.head()

# In[ ]:


# Date in quartile buckets
temp3['CN_CREATED_SHORT'] = '0'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2017-07-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2017-10-01'), 'CN_CREATED_SHORT'] = '2017Q3'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2017-10-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2018-01-01'), 'CN_CREATED_SHORT'] = '2017Q4'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2018-01-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2018-04-01'), 'CN_CREATED_SHORT'] = '2018Q1'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2018-04-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2018-07-01'), 'CN_CREATED_SHORT'] = '2018Q2'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2018-07-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2018-10-01'), 'CN_CREATED_SHORT'] = '2018Q3'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2018-10-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2019-01-01'), 'CN_CREATED_SHORT'] = '2018Q4'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2019-01-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2019-04-01'), 'CN_CREATED_SHORT'] = '2019Q1'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2019-04-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2019-07-01'), 'CN_CREATED_SHORT'] = '2019Q2'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2019-07-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2019-10-01'), 'CN_CREATED_SHORT'] = '2019Q3'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2019-10-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2020-01-01'), 'CN_CREATED_SHORT'] = '2019Q4'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2020-01-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2020-04-01'), 'CN_CREATED_SHORT'] = '2020Q1'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2020-04-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2020-07-01'), 'CN_CREATED_SHORT'] = '2020Q2'
temp3.loc[(temp3['CN_CREATED_AT_DATE'] >= '2019-07-01') & (
            temp3['CN_CREATED_AT_DATE'] < '2020-10-01'), 'CN_CREATED_SHORT'] = '2020Q3'
temp3['CN_CREATED_SHORT'].head()

# In[ ]:


temp3['ROBIN_DEPOT_SHORT'].value_counts().plot(kind='bar')

# ### Sicherungskopie

# In[ ]:


temp4 = temp3.copy()
temp4.head()

# In[ ]:


# Plot Einlagen Volumen Bucket
temp4['VOLUMEN_EINLAGE_SHORT'].value_counts().plot(kind='bar')

# ## Barplots

# In[ ]:


# Multi-dimensional barplots
ggplot(temp4) + aes(x='region', fill='ROBIN_DEPOT_SHORT') + geom_bar() + theme(
    axis_text_x=element_text(angle=45, hjust=1)) + xlab('Region') + ylab('Anzahl') + ggtitle(
    'Anzahl Robindepots nach Region und nach Depotgröße') + labs(fill='Robindepotgröße')

# In[ ]:


# to do: Relative figures for barplots
# zb wie hoch ist der Anteil Einkommen '1-2K' an allen Einkommen der Region Ost

# test_label1 = round(((temp4['EINKOMMEN_SHORT'] == '0')&(temp4['region'] == 'REGION OST')).sum()/(temp4['region'] == 'REGION OST').sum(),4)
# test_label2 = round(((temp4['EINKOMMEN_SHORT'] == '1-2K')&(temp4['region'] == 'REGION OST')).sum()/(temp4['region'] == 'REGION OST').sum(),4)
# test_label3 = round(((temp4['EINKOMMEN_SHORT'] == '2K-4K')&(temp4['region'] == 'REGION OST')).sum()/(temp4['region'] == 'REGION OST').sum(),4)
# test_matrix = np.array([[test_label1, test_label2, test_label3]])
# test_matrix


# In[ ]:


# uni_list1 = temp4['EINKOMMEN_SHORT'].unique()
# uni_list2 = temp4['region'].unique()
# test_label = []
# for i in range(len(uni_list2)):
#    for j in range(len(uni_list1)):
#        print(round(((temp4['EINKOMMEN_SHORT'] == uni_list1[j])&(temp4['region'] == uni_list2[i])).sum()/\
#              (temp4['region'] == uni_list2[i]).sum(),4))
# label_matrix[i] = np.array([[test_label]])


# In[ ]:


# label_matrix = np.zeros((len(uni_list1), len(uni_list2)))
# label_matrix[1] = (1,1,1,1,1,1,1,1,1)
# label_matrix


# In[ ]:


# uni_list1 = temp4['EINKOMMEN_SHORT'].unique()
# uni_list2 = temp4['region'].unique()
# test_label = []
# label_matrix = np.zeros((len(uni_list1), len(uni_list2)))
# for i in range(len(uni_list2)):
#    for j in range(len(uni_list1)):
#        test_label[i] = round(((temp4['EINKOMMEN_SHORT'] == uni_list1[j])&(temp4['region'] == uni_list2[i])).sum()/\
#                              (temp4['region'] == uni_list2[i]).sum(),4)
#        label_matrix[i] = test_label


# In[ ]:


# Multi-dimensional barplots
ggplot(data=temp4) + aes(x='region', fill='EINKOMMEN_SHORT') + geom_bar(mapping=aes(x='region', fill='EINKOMMEN_SHORT'),
                                                                        stat='count') + theme(
    axis_text_x=element_text(angle=45, hjust=1)) + geom_text(
    aes(label='stat(count)', group='EINKOMMEN_SHORT'),
    stat='count',
    # nudge_y=0.125,
    va='center',
    ha='center',
    size=8
    # format_string='{:.1f}%'
)

# In[ ]:


# Multi-dimensional barplots
(ggplot(temp4) + aes(x='region', fill='EINKOMMEN_SHORT') + geom_bar()
 + theme(axis_text_x=element_text(angle=45, hjust=1))
 + geom_text(
            aes(label='stat(prop)*100', group='EINKOMMEN_SHORT'),
            stat='count',
            nudge_y=0.125,
            va='center',
            ha='center',
            size=8,
            format_string='{:.1f}%'
        )
 # + facet_wrap('VOLUMEN_EINLAGE_SHORT')
 )
# +labs(x='Einlagenkonversion', y='Anzahl Einlagenbuckets')


# In[ ]:


# Multi-dimensional barplots
ggplot(temp4) + aes(x='region', fill='VOLUMEN_EINLAGE_SHORT') + geom_bar()
# + facet_wrap('VOLUMEN_EINLAGE_SHORT')\
# +labs(x='Einlagenkonversion', y='Anzahl Einlagenbuckets')


# In[ ]:


# Multi-dimensional barplots
ggplot(temp4) + aes(x='max_depot_ges', fill='ROBIN_DEPOT_SHORT') + geom_bar() + facet_wrap('region')
# + facet_wrap('VOLUMEN_EINLAGE_SHORT')\
# +labs(x='Einlagenkonversion', y='Anzahl Einlagenbuckets')


# In[ ]:


(ggplot(temp4, aes('region', fill='ROBIN_DEPOT_SHORT')) + geom_bar() + theme(
    axis_text_x=element_text(angle=45, hjust=1))
 + geom_text(
            aes(label='stat(prop)*100', group=1),
            stat='count',
            nudge_y=0.125,
            va='bottom',
            format_string='{:.1f}%'))

# ## Boxplots & Density plots

# In[ ]:


# Convert region & alter to factor variable
temp4['region'] = temp4['region'].astype('category')
temp4['ALTER_SHORT'] = temp4['ALTER_SHORT'].astype('category')
temp4['VaR_cat'] = temp4['VaR'].astype('category')
temp4['CN_CREATED_SHORT'] = temp4['CN_CREATED_SHORT'].astype('category')

# In[ ]:


# Boxplots EINLAGEN
ggplot(temp4, aes('region', 'VOLUMEN_EINLAGE')) + geom_boxplot() + theme(axis_text_x=element_text(angle=45, hjust=1))

# In[ ]:


# Density EINLAGEN
ggplot(temp4, aes('PF_TOTAL')) + geom_density() + theme(axis_text_x=element_text(angle=45, hjust=1))

# Since the density distribution of 'VOLUMEN_EINLAGEN', 'VOLUMEN_DEPOT' and 'PF_TOTAL' all were extremly skewed to the right, I transformed them to log returns in order to interprete the graphs

# In[ ]:


# Convert zeros to small values (=0.01) in order to calculate the log
temp4.loc[temp4['VOLUMEN_EINLAGE'] == 0, 'VOLUMEN_EINLAGE_adj'] = 0.01
temp4.loc[temp4['VOLUMEN_EINLAGE'] != 0, 'VOLUMEN_EINLAGE_adj'] = temp4['VOLUMEN_EINLAGE']

temp4.loc[temp4['VOLUMEN_DEPOT'] == 0, 'VOLUMEN_DEPOT_adj'] = 0.01
temp4.loc[temp4['VOLUMEN_DEPOT'] != 0, 'VOLUMEN_DEPOT_adj'] = temp4['VOLUMEN_DEPOT']

temp4.loc[temp4['PF_TOTAL'] == 0, 'PF_TOTAL_adj'] = 0.01
temp4.loc[temp4['PF_TOTAL'] != 0, 'PF_TOTAL_adj'] = temp4['PF_TOTAL']

temp4.loc[temp4['CN_MAX_AMOUNT'] == 0, 'CN_MAX_AMOUNT_adj'] = 0.01
temp4.loc[temp4['CN_MAX_AMOUNT'] != 0, 'CN_MAX_AMOUNT_adj'] = temp4['CN_MAX_AMOUNT']

temp4.loc[temp4['einkommen_echt'] == 0, 'einkommen_echt_adj'] = 0.01
temp4.loc[temp4['einkommen_echt'] != 0, 'einkommen_echt_adj'] = temp4['einkommen_echt']

# In[ ]:


# Log transformation for very skewed distributions
temp4['VOLUMEN_EINLAGE_log'] = np.log10(temp4['VOLUMEN_EINLAGE_adj'])
temp4['VOLUMEN_DEPOT_log'] = np.log10(temp4['VOLUMEN_DEPOT_adj'])
temp4['PF_TOTAL_log'] = np.log10(temp4['PF_TOTAL_adj'])
temp4['CN_MAX_log'] = np.log10(temp4['CN_MAX_AMOUNT_adj'])
temp4['einkommen_echt_log'] = np.log10(temp4['einkommen_echt_adj'])
temp4['einkommen_echt_log'].head()

# In[ ]:


# Density plot with logs
ggplot(temp4, aes('PF_TOTAL_log')) + geom_density() + theme(axis_text_x=element_text(angle=45, hjust=1))

# In[ ]:


# Density plot with logs
ggplot(temp4, aes('PF_TOTAL_log')) + geom_density() + facet_wrap('region') + theme(
    axis_text_x=element_text(angle=45, hjust=1))

# In[ ]:


ggplot(temp4, aes(x='region', y='VOLUMEN_DEPOT_log')) + geom_boxplot() + ylab('Depotvolumen') + xlab('Region') + theme(
    axis_text_x=element_text(angle=45, hjust=1)) + ggtitle('Depots nach Region')

# In[ ]:


# Multi-dimensional boxplot
ggplot(temp4, aes(x='max_einlagen_ges', y='PG10_Sparreserve_vorh')) + geom_boxplot(aes(fill='PG10_Sparreserve_vorh'))

# In[ ]:


# Multi-dimensional boxplot
ggplot(temp4, aes(x='PG10_Sparreserve_vorh', fill='max_einlagen_ges')) + geom_bar(aes(fill='max_einlagen_ges')) + xlab(
    'PG10 Sparreserve vorhanden') + ylab('Anzahl Kunden') + ggtitle('Produkt PG10 & Einlagenkonversion')

# In[ ]:


temp4.columns

# In[ ]:


import seaborn as sns
import matplotlib.pyplot as plt

# Basic correlogram
sns.pairplot(temp4[['PF_TOTAL_log', 'VOLUMEN_DEPOT_log', 'VOLUMEN_EINLAGE_log', 'einkommen_echt_log', 'CN_MAX_log']])

# In[ ]:


# Multi-dimensional boxplot
ggplot(temp4, aes(x='ALTER_SHORT', y='PF_TOTAL_log')) + geom_boxplot(aes(fill='max_einlagen_ges')) + theme(
    axis_text_x=element_text(angle=45, hjust=1))

# In[ ]:


ggplot(temp4) + aes(x='CN_CREATED_AT_DATE', y='VOLUMEN_EINLAGE_log') + geom_boxplot(aes(fill='max_einlagen_ges'),
                                                                                    alpha=0.8) + theme(
    axis_text_x=element_text(angle=45, hjust=1))

# In[ ]:


temp4.columns

# In[ ]:


temp4['PG10_Sparreserve_vorh'].head()

# In[ ]:


# Multi-dimensional barplots>
ggplot(temp4) + aes(x='max_einlagen_ges', y='PF_TOTAL') + geom_boxplot() + facet_wrap('region')

# # Decision Tree

# ## Building the model

# In[ ]:


### label encode the categorical values and convert them to numbers
from sklearn.preprocessing import LabelEncoder

le = LabelEncoder()

le.fit(temp2['ONL_FINANZPLANER'].astype(str))
temp2['ONL_FINANZPLANER'] = le.transform(temp2['ONL_FINANZPLANER'].astype(str))

le.fit(temp2['PROD_MAXBLUE'].astype(str))
temp2['PROD_MAXBLUE'] = le.transform(temp2['PROD_MAXBLUE'].astype(str))

le.fit(temp2['TEL_BANKING_FREISCHALTUNG'].astype(str))
temp2['TEL_BANKING_FREISCHALTUNG'] = le.transform(temp2['TEL_BANKING_FREISCHALTUNG'].astype(str))

le.fit(temp2['region'].astype(str))
temp2['region'] = le.transform(temp2['region'].astype(str))

le.fit(temp2['filiale'].astype(str))
temp2['filiale'] = le.transform(temp2['filiale'].astype(str))

le.fit(temp2['istsegment'].astype(str))
temp2['istsegment'] = le.transform(temp2['istsegment'].astype(str))

le.fit(temp2['GESCHLECHT'].astype(str))
temp2['GESCHLECHT'] = le.transform(temp2['GESCHLECHT'].astype(str))

le.fit(temp2['famst_text'].astype(str))
temp2['famst_text'] = le.transform(temp2['famst_text'].astype(str))

le.fit(temp2['RETURN'].astype(str))
temp2['RETURN'] = le.transform(temp2['RETURN'].astype(str))

# In[ ]:


# Keep relevant columns
data_tree = temp2[['max_einlagen_ges', 'max_freshmoney_ges',
                   'max_depot_ges', 'KFR_KK_GESCHAEFTSAB_12MON_JN',
                   'ONL_BANKING_FREISCHALTUNG', 'ONL_BANKING_NUTZUNG_1M',
                   'ONL_BANKING_NUTZUNG_6M', 'ONL_FINANZPLANER', 'PROD_FZS',
                   'PROD_MAXBLUE', 'PROD_ZINSMARKT', 'SOZI_STUDENT',
                   'TEL_BANKING_FREISCHALTUNG', 'TEL_BANKING_NUTZUNG_1M',
                   'TEL_BANKING_NUTZUNG_6M', 'PG1_Zahlungsverkehr_vorh',
                   'PG2_Kreditkarten_vorh', 'PG3_OBB_inkl_DWS_vorh',
                   'PG4_Verm_Verw_Depots_vorh', 'PG5_Sonst_Depots_vorh',
                   'PG8_Bausparen_vorh', 'PG9_Spareinlagen_vorh', 'PG10_Sparreserve_vorh',
                   'region', 'filiale', 'KUNDENBINDUNG_DAUER', 'ALTER_PARTNER',
                   'VOLUMEN_DEPOT', 'VOLUMEN_EINLAGE', 'einkommen_echt', 'istsegment',
                   'GESCHLECHT', 'famst_text', 'CN_MAX_AMOUNT', 'VaR', 'RETURN',
                   'PF_TOTAL', 'PF_CASH', 'CN_MIN_AMOUNT']]

# In[ ]:


# Training Set
# Randomly select 70% of the data for training
np.random.seed(27)
einlagen_train = data_tree.sample(frac=0.7, random_state=1)
einlagen_train.shape

# In[ ]:


# Using the remaining 30% as testing set
einlagen_test = data_tree.loc[~data_tree.set_index(list(data_tree.columns)).index.isin(
    einlagen_train.set_index(list(einlagen_train.columns)).index)]
einlagen_test.shape

# In[ ]:


# Extract the column max_einlagen_ges which we try to predict
einlagen_test_label = einlagen_test['max_einlagen_ges']
einlagen_test = einlagen_test.iloc[:, 1:]
einlagen_test.shape

# In[ ]:


# Convert floating type to categorical variable
einlagen_train['max_einlagen_ges'] = pd.Categorical(einlagen_train['max_einlagen_ges'])
einlagen_test_label = pd.Categorical(einlagen_test_label)

# ### Simple Decision Tree

# In[ ]:


# Default decision tree model
# Given the datasets the model predicts max_einlagen_ges, thus whether Robins have been converted from clients savings
# np.random.seed(27)
einlagen_features = einlagen_train.columns[1:]
einlagen_dt_clf = DecisionTreeClassifier()
einlagen_dt_clf = einlagen_dt_clf.fit(einlagen_train[einlagen_features], einlagen_train['max_einlagen_ges'])

# In[ ]:


einlagen_dt_clf

# ### Grid Search

# In[ ]:


from sklearn.model_selection import GridSearchCV
# Import the required packages
from sklearn.tree import DecisionTreeClassifier

parameters = {'max_depth': range(3, 20), 'min_samples_leaf': range(2, 6), 'min_samples_split': range(2, 6)}
clf = GridSearchCV(DecisionTreeClassifier(), parameters, n_jobs=4)
clf.fit(X=einlagen_train[einlagen_features], y=einlagen_train['max_einlagen_ges'])
tree_model = clf.best_estimator_

# In[ ]:


# Print the best parameters for the tree
print (clf.best_score_, clf.best_params_)

# ### Decision Tree Grid Search

# In[ ]:


clf.best_params_['max_depth']

# In[ ]:


# Default decision tree model
# Given the datasets the model predicts max_einlagen_ges, thus whether Robins have been converted from clients savings
einlagen_features = einlagen_train.columns[1:]
einlagen_grid_clf = DecisionTreeClassifier(max_depth=clf.best_params_['max_depth'],
                                           min_samples_leaf=clf.best_params_['min_samples_leaf'],
                                           min_samples_split=clf.best_params_['min_samples_split'])
einlagen_grid_clf = einlagen_grid_clf.fit(einlagen_train[einlagen_features], einlagen_train['max_einlagen_ges'])

# ### Random Forest

# In[ ]:


# Import required packages
from sklearn.ensemble import RandomForestClassifier

# Create random forest object classfier
einlagen_rf = RandomForestClassifier(random_state=0)

# Fit the model
einlagen_rf = einlagen_rf.fit(einlagen_train[einlagen_features], einlagen_train['max_einlagen_ges'])

# ## Feature Importance

# In[ ]:


# Create a vector of all column names
# einlagen_features
# Calculate feature importances
importances = einlagen_dt_clf.feature_importances_
# Return the indices of the sorted array
indices = np.argsort(importances)
plt.figure(figsize=(12, 8))
plt.title('Feature Importance', fontsize=20)
plt.xlabel('Relative Importance', fontsize=15)
plt.ylabel('Feature', fontsize=17)
plt.barh(range(len(indices)), importances[indices], align='center')
plt.yticks(range(len(indices)), [einlagen_features[i] for i in indices])
plt.xlabel('Relative Importance')

# ## Model prediction

# In[ ]:


# Models build so far
# Simple Decision Tree: einlagen_dt_clf
# Decision Tree with GridSearch: einlagen_grid_clf
# Random Forest: einlagen_rf


# In[ ]:


# Predict binary values for max_einlagen_ges (1: Einlagen konvertiert, 0: nicht aus Einlagen konvertiert)
einlagen_predict = einlagen_rf.predict(einlagen_test[einlagen_features])
einlagen_predict[:10]

# Can also predict probabilities
# einlagen_predict_proba = einlagen_rf_clf.predict_proba(einlagen_test[einlagen_features])
# einlagen_predict_proba[:10]


# ## Model Evaluation

# In[ ]:


# Calculation of accuracy
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, \
    confusion_matrix

print('F1-Score: {:.4}'.format(f1_score(einlagen_test_label, einlagen_predict)))
print('Precision: {:.4}'.format(precision_score(einlagen_test_label, einlagen_predict)))
print('Recall: {:.4}'.format(recall_score(einlagen_test_label, einlagen_predict)))
print('Accuracy: {:.4}'.format(accuracy_score(einlagen_test_label, einlagen_predict)))

# Calculation of MAE
print('MAE: {:.4}'.format(abs(einlagen_test_label.astype(np.int8) - einlagen_predict).mean()))

# ## Results

# ### Simple Decision Tree: einlagen_dt_clf
# F1-Score: 0.7279
# Precision: 0.7143
# Recall: 0.742
# Accuracy: 0.8297
# MAE: 0.1703

# ### Decision Tree with GridSearch: einlagen_grid_clf
# F1-Score: 0.768
# Precision: 0.7321
# Recall: 0.8075
# Accuracy: 0.8502
# MAE: 0.1498

# ### Random Forest: einlagen_rf
#
# F1-Score: 0.7552
# Precision: 0.7822
# Recall: 0.7299
# Accuracy: 0.8547
# MAE: 0.1453

# In[ ]:


print(confusion_matrix(einlagen_test_label, einlagen_predict))

# ## Visualizing the model

# In[ ]:


# Import the required package
from sklearn import tree

# Visualize the model
# Plot the tree structure
dot_data = tree.export_graphviz(einlagen_dt_clf,
                                out_file=None,
                                feature_names=einlagen_features,
                                class_names='max_einlagen_ges',
                                rounded=True,
                                filled=True)

# In[ ]:


fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(4, 4), dpi=300)
tree.plot_tree(einlagen_dt_clf, feature_names=einlagen_features,
               class_names='max_einlagen_ges', filled=True)
fig.savefig('dt_clf_v1.png')

