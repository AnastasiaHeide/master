import logging
import pandas as pd
import datetime

from Globals.Classes.Oracle_Connector.db_connector import OracleDBConnector as DBCon
from Globals.Classes.Trading_Calendar.trading_calendar import TradingCalendar as tc

logger = logging.getLogger()

# Get data for daily calculations
def view_security_order_rebalancing_contract():
    """
   The method performs an SQL query to the Oracle Database.
   The parameters are the column names, constraints and tables for the SQL query.

   The parameters must be set (preferable as key word arguments) as soon as the method gets invoked.


   :rtype: pandas DataFrame()
   :return: data from the Oracle Database
   """
    connection = DBCon().connect

    days_delta = -1
    yesterday = tc(days_delta).busday

    sql = f"""
        SELECT reb.TX_REQUESTED_AT,reb.TX_ID,reb.TX_CN_ID, pf."VaR", pf.CN_ID, pf.PF_TOTAL, pf.PF_VALUE_DATE, so.SO_ESTIMATED_AMOUNT,
        reb.RB_TRIGGERED_BY_TXT, reb.RB_CI_ID, reb.RB_WI_ID, reb.RB_CR_ID, reb.RB_CC_ID, pf.CN_STATUS_TXT
        FROM VDB238_OWNER.ADM_REBALANCING_TX reb
        INNER JOIN VDB238_OWNER.ADM_CONTRACT_PF pf
        ON pf.CN_ID = reb.TX_CN_ID
        AND to_date(reb.TX_REQUESTED_AT,'dd-mm-yyyy') = to_date(pf.PF_VALUE_DATE,'dd-mm-yyyy')
        INNER JOIN VDB238_OWNER.COLLECTIVE_INSTRUCTION ci
        ON reb.RB_CI_ID = ci.CI_ID
        JOIN VDB238_OWNER.SECURITY_ORDER so
        ON so.SO_RB_ID = reb.TX_ID
        WHERE ci.CI_WORKFLOW_START = to_date('{yesterday}','dd-mm-yyyy')
        AND reb.TX_STATUS_TXT != 'REJECTED'
        AND pf.CN_STATUS_TXT != 'CLOSE_REQUESTED'
        AND pf.CN_STATUS_TXT != 'CLOSE_PREPARED'
        AND reb.RB_TRIGGERED_BY_TXT != 'CONTRACT_ACTIVATED'
        """

    result = pd.read_sql(sql, connection)
    connection.close()
    assert not result.empty, "Dataframe must not be empty. Check the date."

    logger.debug('---------------------------')
    logger.debug(result)
    logger.debug('---------------------------')
    return result

