#install.packages("randomForest")
#install.packages("DescTools")
#install.packages("party")
#install.packages('sjmisc')
#install.packages('glmnet')

library(DescTools)
library(rpart)
library(rpart.plot)
library(randomForest)
library(caret)
library(e1071)
library(party)
library(stringr)
library(sjmisc)
library(glmnet)

#getwd()

setwd('C:/Users/Anastasia Heide/Documents/Projekt_Data Scientist/DataScienceDojo/Titanic')

my_titanic <- read.csv("train.csv", sep=";")
head(my_titanic)

# Replace missing Cabin values with 'Z' 
# Assuming if there is no Cabin given it is the lowest category, therefore assigned with 'Z'
my_titanic$Cabin <- as.character(my_titanic$Cabin)
my_titanic$Cabin[my_titanic$Cabin == ''] <- 'Z'
head(my_titanic)

# Shortening Cabins
for (i in seq(1,nrow(my_titanic))){
    if (str_contains(my_titanic$Cabin[i],"A"))
        my_titanic$Cabin.Short[i] <- "A"
    else if (str_contains(my_titanic$Cabin[i], "B"))
        my_titanic$Cabin.Short[i] <- "B"
    else if (str_contains(my_titanic$Cabin[i], "C"))
        my_titanic$Cabin.Short[i] <- "C"
    else if(str_contains(my_titanic$Cabin[i], "D"))
        my_titanic$Cabin.Short[i] <- "D"
    else if(str_contains(my_titanic$Cabin[i], "E"))
        my_titanic$Cabin.Short[i] <- "E"
    else if(str_contains(my_titanic$Cabin[i], "F"))
        my_titanic$Cabin.Short[i] <- "F"
    else if(str_contains(my_titanic$Cabin[i], "Z"))
        my_titanic$Cabin.Short[i] <- "Z"
}
head(my_titanic)

# Looking for Titles in Names
for (i in seq(1,nrow(my_titanic))){
    if (str_contains(my_titanic$Name[i],"Miss"))
        my_titanic$Title[i] <- "Miss"
    else if (str_contains(my_titanic$Name[i], "Mrs"))
        my_titanic$Title[i] <- "Mrs"
    else if (str_contains(my_titanic$Name[i], "Master"))
        my_titanic$Title[i] <- "Master"
    else if (str_contains(my_titanic$Name[i], "Dr."))
        my_titanic$Title[i] <- "Dr"
    else 
        my_titanic$Title[i] <- "None"
}

# Remove PassengerId, Name, Ticket, and Cabin features
# These are not as useful as other features
my_titanic <- subset(my_titanic, select=-c(PassengerId, Name, Cabin, Ticket))
str(my_titanic)

# Fill nas
my_titanic$Age[is.na(my_titanic$Age)] <- median(my_titanic$Age, na.rm=TRUE)
sum(is.na(my_titanic$Age))

# Convert factor to numerical
my_titanic$Survived <- as.factor(my_titanic$Survived)
my_titanic$Pclass <- as.factor(my_titanic$Pclass)
my_titanic$Cabin.Short <- as.factor(my_titanic$Cabin.Short)
my_titanic$Title <- as.factor(my_titanic$Title)
my_titanic$Embarked <- as.factor(my_titanic$Embarked)

# Build new variable called family
my_titanic$Family <- my_titanic$SibSp + my_titanic$Parch
my_titanic <- subset(my_titanic, select=-c(SibSp, Parch))
head(my_titanic)

# Fare Price relative to Family Size
my_titanic$Fare.Relative <- my_titanic$Fare/(1+my_titanic$Family)
my_titanic <- subset(my_titanic, select=-c(Fare))
head(my_titanic)

summary(my_titanic)

# Split the train data for testing
set.seed(42)
titanic.train.index <- sample(1:nrow(my_titanic), 0.7*nrow(my_titanic))
titanic.train <- my_titanic[titanic.train.index,]
dim(titanic.train)

titanic.test <- my_titanic[-titanic.train.index,]
str(titanic.test)

titanic.lm.model <- glm(Survived ~., data=titanic.train, family="binomial")
print(titanic.lm.model)

summary(titanic.lm.model)

# Fit a regression model to training set
# glmnet requires a matrix input to the model function. 
# See ?glmnet and ?cv.glmnet for the documentation
x.train <- model.matrix(Survived ~ ., data=titanic.train)[,-1] 

# Load the required library
# NOTE: Please ignore the warning message
library(glmnet)
set.seed(42)
# L1 (Lasso) Regularization
# alpha determines the regularization penalty: 1 is lasso, 0 is ridge regression
# glmnet uses cross validation to determine the best lambda value
# binomial for logistic regression 
# intercept = FALSE?
titanic.rl1.model <- cv.glmnet(x.train, titanic.train$Survived, alpha=1, family ="binomial")
print(titanic.rl1.model)
coef(titanic.rl1.model)

summary(titanic.rl1.model)

head(titanic.test[, features])

# Use the optimal model found via CV and make predictions for the training set
features <- c("Pclass", "Sex", "Age","Embarked","Cabin.Short","Title","Family","Fare.Relative")

preds.lasso <- predict(titanic.rl1.model, as.matrix(titanic.test[, features]))

# Display the mean absolute value of the residuals
mean(abs(preds.lasso - titanic.test$Survived))

# Grid search across all possible hyperparameters
tune.grid <- expand.grid(maxdepth=2:4, minsplit=2:5, minbucket=2:5, accuracy=0.00)

# Load the required library
# NOTE: Please ignore the warning message
library(rpart)
for(i in 1:nrow(tune.grid)){
  titanic.model <- rpart(Survived ~., data=titanic.train, control=rpart.control(maxdepth=tune.grid[i,1], minsplit=tune.grid[i,2], minbucket=tune.grid[i,3]))
  titanic.predictions <- predict(titanic.model, titanic.test, type="class")
  titanic.confusion <- table(titanic.predictions, titanic.test$Survived)
  titanic.accuracy <- sum(diag(titanic.confusion)) / sum(titanic.confusion)
  tune.grid[i,4] <- titanic.accuracy
    }


# Select the tuned model with highest accuracy
best.tuned.params <- tune.grid[which.max(tune.grid$accuracy),]
best.tuned.params

titanic.best.tuned.model <- rpart(Survived ~., data=titanic.train, control=rpart.control(maxdepth=best.tuned.params[1,1], minsplit=best.tuned.params[1,2], minbucket=best.tuned.params[1,3]))
print(titanic.best.tuned.model)

# Fit a random forest model to the training set
titanic.rf.model <- randomForest(Survived ~ ., data=titanic.train, importance=TRUE, ntree=500, maxnode=20)

# The randomForest object
print(titanic.rf.model)

importance(titanic.rf.model)
varImpPlot(titanic.rf.model)

#install.packages('e1071')
#install.packages('caret')
#library(caret)
# Set up caret to perform 10-fold cross validation repeated 3 times
caret.control <- trainControl(method = "repeatedcv",
                              number = 10,
                              repeats = 3)

# Cross Validation Classic Parameters

rpart.cv <- train(Survived ~ ., 
                  data = titanic.train,
                  method = "rpart",
                  trControl = caret.control,
                  tuneLength = 15)

# Display the result of the cross validation
rpart.cv

# Cross Validation Classic Parameters

rf.cv <- train(Survived ~ ., 
                  data = titanic.train,
                  method = "rf",
                  trControl = caret.control,
                  tuneLength = 15)

# Display the result of the cross validation
rf.cv

# Define function for accuracy based on tree
accuracy <- function(tree){
    y_predict <- predict(tree, titanic.test, type='class')
    confusion <- table(y_predict, titanic.test$Survived)
    accuracies <- sum(diag(confusion))/sum(confusion)
    return(accuracies)
}

# Precision
precision <- function(tree){
    y_predict <- predict(tree, titanic.test, type='class')
    confusion <- table(y_predict, titanic.test$Survived)
    precision <- confusion[2,2]/sum(confusion[2,])
    return(precision)
}

# Recall function
recall <- function(tree){
    y_predict <- predict(tree, titanic.test, type='class')
    confusion <- table(y_predict, titanic.test$Survived)
    recall <- confusion[2,2]/sum(confusion[,2])
    return(recall)
}

rpart.cv.predict <- predict(rpart.cv, titanic.test, type="raw")
rpart.cv.confusion <- table(rpart.cv.predict, titanic.test$Survived)
accuracy.cv <- sum(diag(rpart.cv.confusion))/sum(rpart.cv.confusion)
precision.cv <- rpart.cv.confusion[2,2]/sum(rpart.cv.confusion[2,])
recall.cv <- rpart.cv.confusion[2,2]/sum(rpart.cv.confusion[,2])

rf.cv.predict <- predict(rf.cv, titanic.test, type="raw")
rf.cv.confusion <- table(rf.cv.predict, titanic.test$Survived)
accuracy.rf.cv <- sum(diag(rf.cv.confusion))/sum(rf.cv.confusion)
precision.cv <- rf.cv.confusion[2,2]/sum(rf.cv.confusion[2,])
recall.cv <- rf.cv.confusion[2,2]/sum(rf.cv.confusion[,2])

accuracy(titanic.rf.model)
accuracy(titanic.best.tuned.model)
accuracy.cv
accuracy.rf.cv

precision(titanic.rf.model)
precision(titanic.best.tuned.model)
precision.cv

recall(titanic.rf.model)
recall(titanic.best.tuned.model)
recall.cv

# Calculate MAE
rf.predict <- as.numeric(predict(titanic.rf.model, titanic.test, type="class"))
best.tuned.predict <- as.numeric(predict(titanic.best.tuned.model, titanic.test, type="class"))

mean(abs(rf.predict - as.numeric(titanic.test$Survived)))
mean(abs(best.tuned.predict - as.numeric(titanic.test$Survived)))
mean(abs(as.numeric(rpart.cv.predict) - as.numeric(titanic.test$Survived)))
mean(abs(as.numeric(rf.cv.predict) - as.numeric(titanic.test$Survived)))

mean(abs(titanic.lm.model$residuals))
mean(abs(titanic.rl1.model$residuals))

titanic.rl1.model$residuals

# Prepare prediction for submission
titanic.test <- read.csv('test.csv')
head(titanic.test)

# Extract Short Cabins
titanic.test$Cabin <- as.character(titanic.test$Cabin)
titanic.test$Cabin[titanic.test$Cabin == ''] <- 'Z'
# Shortening Cabins
for (i in seq(1,nrow(titanic.test))){
    if (str_contains(titanic.test$Cabin[i],"A"))
        titanic.test$Cabin.Short[i] <- "A"
    else if (str_contains(titanic.test$Cabin[i], "B"))
        titanic.test$Cabin.Short[i] <- "B"
    else if (str_contains(titanic.test$Cabin[i], "C"))
        titanic.test$Cabin.Short[i] <- "C"
    else if(str_contains(titanic.test$Cabin[i], "D"))
        titanic.test$Cabin.Short[i] <- "D"
    else if(str_contains(titanic.test$Cabin[i], "E"))
        titanic.test$Cabin.Short[i] <- "E"
    else if(str_contains(titanic.test$Cabin[i], "F"))
        titanic.test$Cabin.Short[i] <- "F"
    else if(str_contains(titanic.test$Cabin[i], "Z"))
        titanic.test$Cabin.Short[i] <- "Z"
}

# Looking for Titles in Names
for (i in seq(1,nrow(titanic.test))){
    if (str_contains(titanic.test$Name[i],"Miss"))
        titanic.test$Title[i] <- "Miss"
    else if (str_contains(titanic.test$Name[i], "Mrs"))
        titanic.test$Title[i] <- "Mrs"
    else if (str_contains(titanic.test$Name[i], "Master"))
        titanic.test$Title[i] <- "Master"
    else if (str_contains(titanic.test$Name[i], "Dr."))
        titanic.test$Title[i] <- "Dr"
    else 
        titanic.test$Title[i] <- "None"
}
head(titanic.test)

# Remove PassengerId, Name, Ticket, and Cabin features
# These are not as useful as other features
test <- subset(titanic.test, select=-c(PassengerId, Name, Ticket, Cabin))

# Convert to factor
test$Pclass <- as.factor(test$Pclass)
test$Cabin.Short <- as.factor(test$Cabin.Short)
test$Embarked <- as.factor(test$Embarked)
test$Title <- as.factor(test$Title)

# Fill missing values
test$Age[is.na(test$Age)] <- median(test$Age, na.rm =TRUE)
test$Fare[is.na(test$Fare)] <- median(test$Fare, na.rm=TRUE)

# New variable family
test$Family <- test$SibSp + test$Parch

# Remove columns SibSp & Parch
test <- subset(test, select=-c(SibSp, Parch))
# Fare Price relative to Family Size
test$Fare.Relative <- test$Fare/(1+test$Family)
test <- subset(test, select=-c(Fare))
head(test)
str(test)

# titanic.rf.model = Random Forest (Accuracy = 0.80,  MAE = .19) 
# titanc.best.tuned.model = Grid Search Decision Tree (Accuracy = 0.83 , MAE = .17)
# rpart.cv = Decision Tree with Cross Validation (Accuracy = 0.78, MAE = .22)

# prediction based on chosen model above
test.predict <- predict(rpart.cv, test, type='raw')
str(test.predict)

# Include PassengerId for submission
submission <- data.frame(PassengerId = titanic.test$PassengerId,
                         Survived = test.predict)
head(submission)

write.table(submission, '07_submission_cv.csv', sep=",", row.names=FALSE)

# Upoad Scores
# titanic.rf.model = 0.79904
# titanic.best.tuned.model = 0.79425
# rpart.cv = 0.79904
